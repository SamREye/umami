External umami calls

## Protocol operations

- wait-for-operation

### Contract operations

- get-balance
- transfer
- multiple-transfers
- set-delegate
- withdraw-delegate
- get-delegate

### Token operations

- check-token-contract
- tokens-transfers
- multiple-tokens-transfers
- get-token-balance

### Wallet operations

- list-known-address
- rename-aliases
- add-address
- forget-address
- gen-keys
- import-from-bmnemonic
- list-known-contracts
- import-secret-keys
- generate-keys-from-mnemonic

### Shell operations

- current-level
