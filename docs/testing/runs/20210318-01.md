# Umami Wallet - Test Run

**Date**: 2021-03-18

## Test Parameters

### Components

| Component | Tag or Branch |
|--|--|
| umami | hd-wallet |
| tezos-indexer | v9.1.0 |
| mezos | indexer-v9-fixes |
| tezos | pierrick@fa1.2 |

### Settings

| Setting | Value |
|--|--|
| Node | https://testnet-tezos.giganode.io/ |
| Mezos | https://qa-api.umamiwallet.com/edo2net |

## Checklist

> Legend: ticking the box means: (1) it completed successfully; or (2) completed partially without defect; or (3) failed but was recoverable. If an issue comes up that is not severe and is recoverable: file an issue, but still tick the box as successful.

### Accounts and Operations

 - [X] Send normal transaction
 - [X] Observe block count and confirmation
 - [ ] Observe explorer, use explorer link
 - [X] Import secret
 - [X] Create derived Account
 - [X] Rename derived Account
 - [X] Send transaction to or from a derived Account
 - [X] Send a Batch transaction
 - [X] Edit a transaction within a Batch
 - [X] Delete a transaction within a Batch
 - [X] Export a secret
 - [X] Rename a secret
 - [X] Delete a secret
 - [X] Select Advanced Options

### Address Book

 - [X] Create alias
 - [X] Observe c&p and QR code
 - [X] Rename alias
 - [X] Delete alias
 - [X] Observe alias in Send form

### Delegations

 - [X] Delegate an Account
 - [X] Observe the delegation
 - [ ] Change baker
 - [X] Delete delegation

### Tokens

 - [X] Register token FA1.2 contract
 - [X] Delete token contract
 - [X] Send token
 - [X] Send token batch

### Settings

 - [X] Change verification blocks setting and Observe
 - [X] Change dark/light mode
 - [X] Reset settings

## Issues

 * #117
 * #118
 * #119
 * #120
 * #121
 * #122
 * #123
 * #124
 * #125
 * #126
