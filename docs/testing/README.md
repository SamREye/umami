# Umami Wallet - Testing Hub

## Glossary

**Candidate**: a Tag which is identified as a potential Release, assuming tests prove its fitness

**Checklist**: a list of tests that serve as criteria for Fitness

**Fit or Fitness**: to have sufficient proven quality, making it appropriate for public use

**Release**: a Tag which is shown to be fit for purpose

**Results, Test**: an instance of a checklist, as verified on a Candidate

**Tag**: a label (i.e. means to identify) a specific commit (or a build from a commit)

**Test (Run)**: to evaluate a Candidate, resulting in a Test Result

**Version**: either a tag, a candidate or a release, identifiable by a tag (or version number)

