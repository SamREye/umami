# Ref-Wallet Project Charter

## Goals

 * To have a Wallet application that supports all core features of Nodes (that are relevant to wallets)
 * To have a Wallet application that supports all parameters--even arcane ones--for operations
 * FUTURE: to serve as a staple for standard setting

## Business Need

The Tezos ecosystem lacks a comprehensive wallet that covers all features available from the Nodes.

## Additional Details

 * [Architecture](/docs/operative/architecture.md)
 * [Roster](/docs/operative/roster.md)
 * [History](/docs/operative/history.md)
 * [Project Roadmap](/docs/operative/project_plan.md)
 * [Product Roadmap](/docs/operative/product_plan.md)
 * [Risk Register](/docs/operative/risks.md)
 * [Backlog](/docs/operative/backlog.md)
 