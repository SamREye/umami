# Ref-Wallet - Roster

Here's a list of contributors and their corresponding title/roles:

| Name | Title/Role | Area | Description |
|--|--|--|--|
| Samuel Bourque | Product Manager | Management | General oversight of project and product |
| Remy El Sibaie | Senior Developer - Tech Lead | Core Wallet & Tezos SDK | Development oversight |
| Pierrick Couderc | Senior Developer | Core Wallet & Tezos SDK | Tokenization and Tezos SDK |
| Philippe Wang | Senior Developer | Indexer | Indexer improvements |
| Steve Sanches | Developer | Core Wallet | Core development and integration |
| Nicolas Fillion | Developer | Testing | Automated testing and CI |
| Julia Ah-Yionne | Designer | UX | Mockups, User flows, User feedback |
| Alexandre Doussot | Developer | Node/Shell/Michelson | Tezos Client, libclient improvements |
| Corentin Mehat | DevOps | Infrastructure | Deployments and versioning |
| Freddy Harris | Developer | UI Programing | Integrating UI to codebase |
