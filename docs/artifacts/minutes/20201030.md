# Nomadic Labs - Reference Wallet Project - Meeting Minutes

**Held on**: 2020-10-30 at 11:30 CEST

## Attendees

 * Arthur Breitman
 * Michel Mauny
 * Oana Piciorus Ladret
 * Samuel Bourque
 * Remy El Sibaie
 * Steve Sanches
 * Nicolas Fillion
 * Julia Ah-Yionne
 * Alexandre Doussaut

## Minutes

 * [SB] Recap of previous meeting, agenda and follow ups
 * [SB] Report for Sprint 7:
     * General Optimizations: faster tx, faster refresh
     * Transaction form enhancements
     * Address Book integration
     * New members: Remy (2 weeks ago) and Pierrick (next week)
 * Demo:
     * [SS] new transaction flow w/ advanced options
     * [SS] Address book
     * [JA] new mockup for onboarding
 * [AB] Feedback:
     * Instant history
     * start at 0 blocks in confirmation
     * localization
     * error console
     * simple title
     * freestyle sender entry
     * show sender balances
     * show unit in amounts
     * BIP39 password use
     * ledger -- multiple master keys
     * distinguish address not in blockchain
     * DirectAuth

## Follow ups

 * [see feedback]
