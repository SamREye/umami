[@react.component]
let make = () => {
  <Page>
    <VerificationSettingView />
    <ThemeSettingView />
    <ChainSettingView />
    <DangerSettingView />
  </Page>;
};
