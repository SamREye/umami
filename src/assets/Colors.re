module Dark = {
  let maxEmphasis = "#FFF";
  let highEmphasis = "rgba(255,255,255,0.87)";
  let mediumEmphasis = "rgba(255,255,255,0.6)";
  let disabled = "rgba(255,255,255,0.38)";

  // for amount data
  let positive = "#76E59B";
  let negative = "#f97977";

  let primary = "#f74f18";

  let background = "#121212";
  let barBackground = "#212121";
  let scrim = "rgba(92,92,92,0.32)";

  let elevatedBackground = "#212121";

  let statePressed = "rgba(255, 255, 255, 0.1)";
  let stateHovered = "rgba(255, 255, 255, 0.04)";
  let stateActive = "rgba(255, 255, 255, 0.08)";
};

module Light = {
  let maxEmphasis = "#000";
  let highEmphasis = "rgba(0,0,0,0.87)";
  let mediumEmphasis = "rgba(0,0,0,0.6)";
  let disabled = "rgba(0,0,0,0.38)";

  // for amount data
  let positive = "#55a388";
  let negative = "#d46250";

  let primary = "#f74f18";

  let background = "#f9f9f9";
  let barBackground = "#FFF";
  let scrim = "rgba(153,145,134,0.32)";

  let statePressed = "rgba(0, 0, 0, 0.1)";
  let stateHovered = "rgba(0, 0, 0, 0.04)";
  let stateActive = "rgba(0, 0, 0, 0.08)";
};

// for application state like form
let error = "#BB3838";
let valid = "#76E59B";
