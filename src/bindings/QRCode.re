[@bs.module "./react-native-qrcode-svg"] [@react.component]
external make: (~value: string, ~size: float) => React.element = "default";
